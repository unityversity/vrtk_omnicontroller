﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A wearable container that you can move around and potentially wear or take off as a back, shoulder or belt container.
/// </summary>
public class VRTKWearableContainer : MonoBehaviour {

    /// <summary>
    /// Can the item be put on
    /// </summary>
    public bool wearable = true;
    /// <summary>
    /// Can the item be unworn
    /// </summary>
    public bool removeable = true;
    /// <summary>
    /// Snappables can attach to modular loops on existing items such as belts and modular bags/rigs
    /// </summary>
    public bool snappable = false;
    /// <summary>
    /// Snaps lock it into place and cannot be removed until they are unshapped with long grabbing and pulling apart at the bottom
    /// </summary>
    /// <remarks>
    /// Multiple snaps exist but all will unsnap at once for now.
    /// </remarks>
    public bool snapLocked = false;
    /// <summary>
    /// Snaps shut but non-removeable
    /// </summary>
    public bool snapShut = false;
    /// <summary>
    /// The game object that is the snap locking interactble
    /// </summary>
    public GameObject snapGameObject;

    public enum WearingPosition
    {
        /// <summary>
        /// Worn on back and must be removed to access its contents.
        /// </summary>
        Backpack,
        /// <summary>
        /// Worn over the shoulder and can be rotated to the front for quick access
        /// </summary>
        ShoulderBag,
        /// <summary>
        /// Worn in a fixed position on the hip attached to clothing other than a belt.
        /// </summary>
        HipPocket,
        /// <summary>
        /// Worn in a fixed position on the leg attached to clothing other than a belt.
        /// </summary>
        LegPocket,
        /// <summary>
        /// A container worn on the belt. Can slide until it collides with other belt items. Detachable by snaps.
        /// </summary>
        BeltPouch,
        /// <summary>
        /// Worn on the modular attachment points of a vest. Detachable by snaps.
        /// </summary>
        VestPouch,
        /// <summary>
        /// Can be worn on back or front. To swap, you must remove and use both hands to rotate before putting it back on the opposite side. You could wear two backpacks at once but the top one must be removed before the first one when layering shoulder straps.
        /// </summary>
        BackOrFrontPackPack
    }


    /// <summary>
    /// Containers must freeze when you manipulate or are about to manipulate their contents and unfreeze when you cease doing so.
    /// </summary>
    public enum WearableContainerState
    {
        /// <summary>
        /// Initially, shoulder bag is worn behind you
        /// </summary>
        MovingClosedStowed,
        /// <summary>
        /// The state when you grab the strap and are pulling it forwards or backwards to one of two dropzone targets
        /// </summary>
        MovingClosedStowedGrabbed,
        /// <summary>
        /// Pulling on the strap rotates it to the front so you can access it
        /// </summary>
        MovingClosedFront,
        /// <summary>
        /// When you have moved it to your front and opened it but not touched anything inside, allowing you look while moving
        /// </summary>
        MovingOpenFront,
        /// <summary>
        /// When you have it open and it is locked into place for physics safety once you manipulate contents or have another grabbed item you might put in. It will snap shut and back to you and unfreeze when you turn or move much
        /// </summary>
        FrozenOpenFront,
        /// <summary>
        /// When you grab with two hands to take it on or off by lifting over your head and forwards
        /// </summary>
        MovingClosedDoubleGrabbed,
        /// <summary>
        /// When you release it not over your head and it falls in the world
        /// </summary>
        DroppedClosed,
        /// <summary>
        /// When you open a stationary nonworn container
        /// </summary>
        DroppedOpen,
        /// <summary>
        /// Container freezes when you touch its contents or have another object held near it to put in
        /// </summary>
        DroppedFrozenOpen

    }
    public Transform contentsParentTransform;
    public List<GameObject> contents;
    public void FreezeContents()
    {
        foreach (GameObject contentItem in contents)
        {
            FreezeGameObject(contentItem);
        }
    }

    public void FreezeGameObject(GameObject objectToFreeze)
    {
        Rigidbody rb = objectToFreeze.GetComponent<Rigidbody>();
        if (rb != null)
        {
            rb.isKinematic = true;
            objectToFreeze.transform.parent = contentsParentTransform;
        }
    }

    public void UnfreezeGameObject(GameObject objectToUnfreeze)
    {
        Rigidbody rb = objectToUnfreeze.GetComponent<Rigidbody>();
        if (rb != null)
        {
            rb.isKinematic = false;
            objectToUnfreeze.transform.parent = null;
        }
    }


	// Use this for initialization
	void Start () {
		if(contentsParentTransform==null)
        {
            Debug.Log("Set contentsParentTransform.");
            contentsParentTransform = this.gameObject.transform;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
