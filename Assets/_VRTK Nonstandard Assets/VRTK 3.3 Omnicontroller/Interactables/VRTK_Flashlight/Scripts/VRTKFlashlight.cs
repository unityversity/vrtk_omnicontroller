﻿//namespace VRTK.Examples
//{
using UnityEngine;
using VRTK;

public class VRTKFlashlight : VRTK.VRTK_InteractableObject
{
    public bool switchedOn = true;
    public GameObject toggledLightOnObject;
    public GameObject toggledLightOffObject;
    public AudioClip[] toggleClickClips;
    public AudioSource source;

    public void PlayRandomClip(AudioClip[] clips)
    {
        if (clips != null && clips.Length > 0)
        {
            
            if (source == null)
            {
                GameObject sourceObject = new GameObject();
                sourceObject.name = gameObject.name + " AudioSource";
                sourceObject.transform.parent = gameObject.transform;
                sourceObject.transform.localPosition = Vector3.zero;
                source = sourceObject.AddComponent<AudioSource>();
                source.spatialBlend = 1f;
                source.loop = false;
            }
            else
            {
                
                source = gameObject.GetComponent<AudioSource>();
            }
            source.PlayOneShot(clips[Random.Range(0, clips.Length - 1)], 1f);
        }
        else
        {
            Debug.Log("No clips to play");
        }
    }
    public override void StartUsing(VRTK_InteractUse usingObject)
    {
        base.StartUsing(usingObject);
        ToggleFlashlight();
    }

    private void ToggleFlashlight()
    {
        switchedOn = !switchedOn;
        //GetComponentInChildren<Light>().enabled = !GetComponentInChildren<Light>().enabled;
        SetOnOffObjects();
        PlayRandomClip(toggleClickClips);
    }

    private void SetOnOffObjects()
    {
        toggledLightOffObject.SetActive(!switchedOn);
        toggledLightOnObject.SetActive(switchedOn);
    }

    private void Start()
    {
        SetOnOffObjects();
    }
}
//}