﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// using Lines drawing code at: https://raw.githubusercontent.com/mvaganov/galactus/master/galactus/Assets/Nonstandard%20Assets/Lines.cs

[RequireComponent(typeof(Rigidbody))]
public class RBAgent : MonoBehaviour {

	private Rigidbody rb;
	public Vector3 targetLocation;
	public float speed = 3;
	public bool moveToTarget = true;
	GameObject line_target, line_dir;

	[System.Serializable]
	public struct RandomWalkDetails {
		public bool enabled;
		public float duration;
		public float durationWiggleRoom;
		[HideInInspector]
		public float timer;
		public void Update(System.Action action){
			timer += Time.deltaTime;
			if(timer > duration) {
				timer -= duration;
				timer -= Random.Range(-durationWiggleRoom, durationWiggleRoom);
				action.Invoke();
			}
		}
		public void QueueUpdate() { timer = duration; }
	}
	public RandomWalkDetails randomWalk = new RandomWalkDetails {
		enabled = true, duration = 2, durationWiggleRoom = 0.5f
	};
	[Tooltip("if clear, will not draw debug lines")]
	public Color debugLineColor = Color.red;

	private void Start() {
		rb = GetComponent<Rigidbody>();
		rb.freezeRotation = true;
		randomWalk.QueueUpdate();
	}

	public void PickANewTarget() {
		Vector3 direction = Random.onUnitSphere;
		direction.y = 0;
		direction.Normalize();
		targetLocation = transform.position + direction * speed * randomWalk.duration * transform.lossyScale.y;
		if(debugLineColor.a > 0) {
			NS.Lines.MakeCircle(ref line_dir, targetLocation, Vector3.up, debugLineColor, transform.lossyScale.y, 24, 0.125f * transform.lossyScale.y);
		}
	}

	void Update () {
        VRTK.VRTK_InteractableObject interact = GetComponent<VRTK.VRTK_InteractableObject>();
        if(interact != null && interact.IsGrabbed()) { return; }

		if(randomWalk.enabled) {
			randomWalk.Update(PickANewTarget);
		}
		if(moveToTarget) {
			Vector3 delta = targetLocation - transform.position;
			float dist = delta.magnitude;
			Vector3 dir = delta / dist;
			dir.y = 0; // this only works because gravity is down.
			dir.Normalize();
			if(dir != Vector3.zero) {
				Quaternion targetRotation = Quaternion.LookRotation(dir, transform.up);
				float gravityAmount = Vector3.Dot(rb.velocity, Vector3.down);
				transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, 0.25f);
                rb.velocity = (dir * speed * transform.lossyScale.y) + Vector3.down * gravityAmount;
				if(debugLineColor.a > 0) {
                    NS.Lines.MakeArrow(ref line_target, transform.position, transform.position + dir * transform.lossyScale.y, debugLineColor, 0.125f*transform.lossyScale.y, 0.125f * transform.lossyScale.y);
				}
			}
		}
	}
}
