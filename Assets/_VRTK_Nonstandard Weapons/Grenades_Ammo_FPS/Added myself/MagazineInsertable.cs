﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagazineInsertable : MonoBehaviour {
    /// <summary>
    /// Inserted into a weapon
    /// </summary>
    public bool inserted = false;
    /// <summary>
    /// Contains rounds already, non-empty. New rounds push old ones down till it hits max capacity. This might need to be handled specially to make it appear a round is there while emptying the dropzone to allow another item to be dropped.
    /// </summary>
    public bool containsRounds = false;

    public void InsertMag()
    {
        inserted = true;
        GetComponentInChildren<VRTK.VRTK_SnapDropZone>().enabled = false;
    }

    public void RemoveMag()
    {
        inserted = false;
        GetComponentInChildren<VRTK.VRTK_SnapDropZone>().enabled = true;
    }
    // Use this for initialization
    void Start()
    {
        if (inserted)
        {
            InsertMag();
        }
        else
        {
            RemoveMag();
        }
    }
	
}
