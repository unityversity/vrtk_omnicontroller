﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

/// <summary>
/// Simulates temperature
/// </summary>
/// <remarks>
/// In physics, everything must have an equation! It's kind of an unwritten rule. Conduction is no exception. How fast conduction happens depends on several factors: what material the objects are made from (the conductivity), the surface area of the two objects in contact, the difference in temperature between the two objects, and the thicknesses of the two objects.
/// In equation form, it looks like this.
/// Q/t = (kA(T2 - T1)) / d
/// Q over t is the rate of heat transfer - the amount of heat transferred per second, measured in Joules per second, or Watts. k is the thermal conductivity of the material - for example, copper has a thermal conductivity of 390, but wool has a thermal conductivity of just 0.04. T1 is the temperature of one object, and T2 is the temperature of the other.Since it's a temperature difference, you can actually use Celsius or Kelvin, whichever is most convenient. And d is the thickness of the material we're interested in.
/// So the rate of heat transfer to an object is equal to the thermal conductivity of the material the object is made from, multiplied by the surface area in contact, multiplied by the difference in temperature between the two objects, divided by the thickness of the material.
/// </remarks>
public class Temperature : MonoBehaviour {

    public enum Temp
    {
        Hot,
        Cold
    }
    public Temp temp = Temp.Hot;
    public float temperature = 23f;
    private float roomTemperature = 23f;
    public float roomTemperatureCoolDegreesPerSecond = 0.5f;
    public float cookingTemperature;
    public float burningTemperature = 60f;

    public Renderer renderer;
    public bool debugTemperatureColor = false;
    public Gradient heatColorGradient;

    public VRTK.Controllables.PhysicsBased.VRTK_PhysicsRotator rotatorControl;

    /// <summary>
    /// Continually generates heat to reach a target temperature.
    /// </summary>
    public bool heatGenerator;
    public float heatGeneratorTarget = 50f;
    public float heatGeneratorDegreesPerSecond = 2f;
    public float heatGeneratorTargetMax = 50f;

    /// <summary>
    /// If we're in colliison with other objects, try to transfer heat
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionStay(Collision collision)
    {
        GameObject other = collision.gameObject;
        Temperature otherTemperature = other.GetComponent<Temperature>();
        if (otherTemperature != null)
        {
            if(temp==Temp.Hot)
            {
                otherTemperature.temp = Temp.Hot;
            }
            if (temperature > otherTemperature.temperature)
            {
               float difference = temperature - otherTemperature.temperature;
               float transferFactor = 0.5f; // surface area and thickness
               otherTemperature.temperature += heatGeneratorDegreesPerSecond  * Time.deltaTime * transferFactor;
            }
            if(temperature < otherTemperature.temperature)
            {

            }
        }
    }
    // Use this for initialization
    void Start () {
        renderer = GetComponent<Renderer>();	
	}

    public float rotatorControlTargetMultiple = 1f;

	// Update is called once per frame
	void Update ()
    {
        if(rotatorControl!=null)
        {
           heatGeneratorTarget = rotatorControl.GetValue() * rotatorControlTargetMultiple;
        }
		if (heatGenerator)
        {
            if(temperature <= heatGeneratorTarget)
            {
                temperature += heatGeneratorDegreesPerSecond * Time.deltaTime;
            }
        }
        else
        {
            if (temperature >= roomTemperature)
            {
                temperature -= roomTemperatureCoolDegreesPerSecond * Time.deltaTime;
            }
            
            if(temperature <= roomTemperature)
            {
                temperature += roomTemperatureCoolDegreesPerSecond * Time.deltaTime;
            }
        }
        if (debugTemperatureColor)
        {
            float temperatureScaled = (temperature)/50f;
            renderer.material.color = heatColorGradient.Evaluate(temperatureScaled); //new Color(temperatureScaled, 0f, -temperatureScaled);
        }

        if (temperature > burningTemperature && !burned)
        {

                burned = true;
            if (smokeEmitterPrefab != null)
            {
                smokeEmitter = Instantiate(smokeEmitterPrefab, transform.position, Quaternion.identity, transform);
                smokeEmitter.SetActive(true);
            }
        }

        if (burned && temperature < (burningTemperature / 2f))
        {
            if (smokeEmitter != null)
            {
                smokeEmitter.SetActive(false);
            }
        }
    }
    public bool burned = false;
    public GameObject smokeEmitter;
    public GameObject smokeEmitterPrefab;
}
