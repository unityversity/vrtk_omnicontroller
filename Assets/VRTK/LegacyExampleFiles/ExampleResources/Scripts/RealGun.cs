﻿namespace VRTK.Examples
{
    using UnityEngine;
    using System.Collections.Generic;

    public class RealGun : VRTK_InteractableObject
    {
        public float bulletSpeed = 200f;
        public float bulletLife = 5f;

        private GameObject bullet;
        private GameObject trigger;
        private RealGun_Slide slide;
        private RealGun_SafetySwitch safetySwitch;

        private Rigidbody slideRigidbody;
        private Collider slideCollider;
        private Rigidbody safetySwitchRigidbody;
        private Collider safetySwitchCollider;

        private VRTK_ControllerEvents controllerEvents;

        private float minTriggerRotation = -10f;
        private float maxTriggerRotation = 45f;

        public AudioClip[] fireClips;
        public AudioClip[] magReleaseClips;
        public AudioClip[] magInsertClips;
        public AudioClip[] emptyClickClips;
        public AudioClip[] slidePullbackClips;
        public AudioClip[] slideReleaseClips;
        public AudioSource source;

        public bool roundChambered = true;
        public bool roundJammed = false;
        public Round round;

        public Magazine insertedMagazine = new Magazine();
        public bool magazineLocked = true;
        public bool magazineEmpty = false;
        public void ReleaseMagazine()
        {
            magazineLocked = false;
        }

        public List<Round> roundTypes = new List<Round>();
        [System.Serializable]
        public class Round
        {
            public string caliber = "9mm";

            /// <summary>
            /// Recoil measured in mm muzzle climb, reduced by weapon mass and other reducers
            /// </summary>
            public float feltRecoilMuzzleClimbMM = 1f;

            float targetTorsoIncapacitationRate = 0.5f;
            float targetHeadIncapacitationRate = 0.75f;
            float extremityIncapacitationRate = 0.3f;
            /// <summary>
            /// Rated in fps, to convert into mps
            /// </summary>
            float muzzleVelocityFPS = 1500f;
            /// <summary>
            /// Rated in grains, to convert into kg
            /// </summary>
            float projectileMassGrains = 55f;
            /// <summary>
            /// barrier penetration factor as a multiple of its effective fps/mass energy
            /// </summary>
            public float penetrationMultiple = 0.5f;
            /// <summary>
            /// expansion of the wound channel as a multiple of the bullet's diameter
            /// </summary>
            public float expansionMultiple = 1.5f;
            public GameObject roundPrefab;
            public GameObject casingPrefab;
            public GameObject projectilePrefab;
            public AudioClip[] fireClips;
            public AudioClip[] impactClips;
            public AudioClip[] casingBounceClips;
        }
        public Transform ejectionPoint;
        public float ejectionForce = 100f;
        //public float ejectionRotation = Vector3.left * 100f;

        [System.Serializable]
        public class Magazine
        {
            /// <summary>
            /// Corresponds to a type of ammo
            /// </summary>
            public string caliber = "9mm";
            public GameObject roundPrefab;
            public GameObject casingPrefab;
            public GameObject projectilePrefab;
            /// <summary>
            /// Current number of rounds in magazine 
            /// </summary>
            int count = 20;
            /// <summary>
            /// Max capacity of magazine + 1 in chamber
            /// </summary>
            int capacity = 20;
            /// <summary>
            /// Jammed feed requires magazine removal and clearing the next round before reinserting magazine. FTF can occur when ammo/mag/gas system are mismatched and cycle/feed incorrectly.
            /// </summary>
            bool jammedFeed = false;
            /// <summary>
            /// The chance out of 10000 for a failure to feed
            /// </summary>
            float magazineFeedFailureRate = 0f;
            
        }

        /// <summary>
        /// Ratings of various barriers in terms of stopping power.
        /// </summary>
        public enum BarrierPenetrationResistance
        {
            Wood = 100,
            Metal = 150,
            Stone = 250,
            Plaster = 60,
            Concrete = 120,
            Paper = 50,
            Plastic = 75,
            Glass = 60
        }
        
        private void ToggleCollision(Rigidbody objRB, Collider objCol, bool state)
        {
            objRB.isKinematic = state;
            objCol.isTrigger = state;
        }

        private void ToggleSlide(bool state)
        {
            if (!state)
            {
                slide.ForceStopInteracting();
            }
            slide.enabled = state;
            slide.isGrabbable = state;
            ToggleCollision(slideRigidbody, slideCollider, state);
        }

        private void ToggleSafetySwitch(bool state)
        {
            if (!state)
            {
                safetySwitch.ForceStopInteracting();
            }
            ToggleCollision(safetySwitchRigidbody, safetySwitchCollider, state);
        }

        public override void Grabbed(VRTK_InteractGrab currentGrabbingObject)
        {
            base.Grabbed(currentGrabbingObject);

            controllerEvents = currentGrabbingObject.GetComponent<VRTK_ControllerEvents>();

            ToggleSlide(true);
            ToggleSafetySwitch(true);

            //Limit hands grabbing when picked up
            if (VRTK_DeviceFinder.GetControllerHand(currentGrabbingObject.controllerEvents.gameObject) == SDK_BaseController.ControllerHand.Left)
            {
                allowedTouchControllers = AllowedController.LeftOnly;
                allowedUseControllers = AllowedController.LeftOnly;
                slide.allowedGrabControllers = AllowedController.RightOnly;
                safetySwitch.allowedGrabControllers = AllowedController.RightOnly;
            }
            else if (VRTK_DeviceFinder.GetControllerHand(currentGrabbingObject.controllerEvents.gameObject) == SDK_BaseController.ControllerHand.Right)
            {
                allowedTouchControllers = AllowedController.RightOnly;
                allowedUseControllers = AllowedController.RightOnly;
                slide.allowedGrabControllers = AllowedController.LeftOnly;
                safetySwitch.allowedGrabControllers = AllowedController.LeftOnly;
            }
        }

        public override void Ungrabbed(VRTK_InteractGrab previousGrabbingObject)
        {
            base.Ungrabbed(previousGrabbingObject);

            ToggleSlide(false);
            ToggleSafetySwitch(false);

            //Unlimit hands
            allowedTouchControllers = AllowedController.Both;
            allowedUseControllers = AllowedController.Both;
            slide.allowedGrabControllers = AllowedController.Both;
            safetySwitch.allowedGrabControllers = AllowedController.Both;

            controllerEvents = null;
        }

        public override void StartUsing(VRTK_InteractUse currentUsingObject)
        {
            base.StartUsing(currentUsingObject);
            if (safetySwitch.safetyOff)
            {
                slide.Fire();
                FireBullet();
                VRTK_ControllerHaptics.TriggerHapticPulse(VRTK_ControllerReference.GetControllerReference(controllerEvents.gameObject), 0.63f, 0.2f, 0.01f);
                
            }
            else
            {
                VRTK_ControllerHaptics.TriggerHapticPulse(VRTK_ControllerReference.GetControllerReference(controllerEvents.gameObject), 0.08f, 0.1f, 0.01f);
            }
        }

        protected override void Awake()
        {
            base.Awake();
            bullet = transform.Find("Bullet").gameObject;
            bullet.SetActive(false);

            trigger = transform.Find("TriggerHolder").gameObject;

            slide = transform.Find("Slide").GetComponent<RealGun_Slide>();
            slideRigidbody = slide.GetComponent<Rigidbody>();
            slideCollider = slide.GetComponent<Collider>();

            safetySwitch = transform.Find("SafetySwitch").GetComponent<RealGun_SafetySwitch>();
            safetySwitchRigidbody = safetySwitch.GetComponent<Rigidbody>();
            safetySwitchCollider = safetySwitch.GetComponent<Collider>();

            ejectionPoint = transform.Find("EjectionPoint");
            // ADDED
            if (source == null)
            {
                AudioSource foundSource = GetComponent<AudioSource>();
                if (source == null)
                {
                    source = gameObject.AddComponent<AudioSource>();
                    source.spatialBlend = 1f;
                    source.loop = false;
                }
            }
        }

        protected override void Update()
        {
            base.Update();
            if (controllerEvents)
            {
                var pressure = (maxTriggerRotation * controllerEvents.GetTriggerAxis()) - minTriggerRotation;
                trigger.transform.localEulerAngles = new Vector3(0f, pressure, 0f);
            }
            else
            {
                trigger.transform.localEulerAngles = new Vector3(0f, minTriggerRotation, 0f);
            }
        }

        private void FireBullet()
        {
            RandomClip(source, fireClips);
            GameObject bulletClone = Instantiate(bullet, bullet.transform.position, bullet.transform.rotation) as GameObject;
            bulletClone.SetActive(true);
            Rigidbody rb = bulletClone.GetComponent<Rigidbody>();
            rb.AddForce(bullet.transform.forward * bulletSpeed);
            Destroy(bulletClone, bulletLife);
            GameObject casingClone = Instantiate(insertedMagazine.casingPrefab, ejectionPoint.position, Quaternion.Euler(ejectionPoint.forward)) as GameObject;
            casingClone.GetComponent<Rigidbody>().AddForce(ejectionPoint.forward * ejectionForce);

        }

        // ADDED
        private void RandomClip(AudioSource sourcePlaying, AudioClip[] clips)
        {
            sourcePlaying.PlayOneShot(clips[Random.Range(0, clips.Length - 1)]);
        }
    }
}