﻿namespace VRTK.Examples
{
    using UnityEngine;
    using UnityEngine.UI;
    using VRTK.Controllables;

    public class ToggleMessageGameObject : MonoBehaviour
    {
        public VRTK_BaseControllable controllable;
        public Text displayText;
        public Text descriptionText;
        public GameObject toggleObject;
        public string onText = "On";
        public string offText = "Off";
        public string description = "";
        // Added to the original script from VRTK
        public bool sendMessage = false;
        public string sendMessageNameMin = "ActivateTrigger";
        public string sendMessageNameMax = "";
        public string sendMessageArg = "";
        public bool toggleOnMin = true;
        public bool toggleOnMax = false;
        public bool toggleOpposite = true;
        public float minToggleInterval = 0.05f;
        private float timeToggledLast = 0f;
        protected VRTK_InteractableObject io;

        protected virtual void OnEnable()
        {
            if (controllable != null)
            {
                controllable.MaxLimitReached += MaxLimitReached;
                controllable.MinLimitReached += MinLimitReached;
            }
            Invoke("SetupIOListeners", 0.1f);
        }

        protected virtual void OnDisable()
        {
            if (controllable != null)
            {
                controllable.MaxLimitReached -= MaxLimitReached;
                controllable.MinLimitReached -= MinLimitReached;
            }

            if (io != null)
            {
                io.InteractableObjectTouched -= InteractableObjectTouched;
            }
        }

        protected virtual void SetupIOListeners()
        {
            io = controllable.GetComponentInParent<VRTK_InteractableObject>();
            if (io != null)
            {
                io.InteractableObjectTouched += InteractableObjectTouched;
            }
        }

        protected virtual void MinLimitReached(object sender, ControllableEventArgs e)
        {
            if (Time.time - timeToggledLast > minToggleInterval)
            {
                if (sendMessageNameMin != "")
                {
                    toggleObject.SendMessage(sendMessageNameMin, SendMessageOptions.DontRequireReceiver);
                }
                if (toggleOnMin)
                {
                    ToggleObject(!toggleObject.activeSelf);
                    UpdateText(offText);
                }
            }
        }

        protected virtual void MaxLimitReached(object sender, ControllableEventArgs e)
        {
            if (Time.time - timeToggledLast > minToggleInterval)
            { 
                if (sendMessageNameMax != "")
                {
                    toggleObject.SendMessage(sendMessageNameMax, SendMessageOptions.DontRequireReceiver);
                }
                if (toggleOnMax)
                {
                    ToggleObject(!toggleObject.activeSelf);
                    UpdateText(onText);
                }
            }
        }

        protected virtual void InteractableObjectTouched(object sender, InteractableObjectEventArgs e)
        {
            if (descriptionText != null)
            {
                descriptionText.text = description;
            }
        }

        protected virtual void ToggleObject(bool state)
        {
            if (toggleObject != null)
            {
                toggleObject.SetActive(state);
            }
        }

        protected virtual void UpdateText(string text)
        {
            if (displayText != null)
            {
                displayText.text = text;
            }
        }
    }
}